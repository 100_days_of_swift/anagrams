//
//  WordsTableViewController.swift
//  Anagrams
//
//  Created by Hariharan S on 10/05/24.
//

import UIKit

class WordsTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    var allWords = [String]()
    var usedWords = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadStringsFromFile()
        self.configureNavBar()
        self.startGame()
    }
}

// MARK: - Private Methods

private extension WordsTableViewController {
    func loadStringsFromFile() {
        if let wordsFileURL = Bundle.main.url(
            forResource: "words",
            withExtension: "txt"
        ) {
            if let words = try? String(contentsOf: wordsFileURL) {
                self.allWords = words.components(separatedBy: "\n")
            }
        }
    }
    
    func configureNavBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(self.promptForAnswer)
        )
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(self.startGame)
        )
    }
    
    func submit(_ answer: String) {
        let lowerAnswer = answer.lowercased()
        if isPossible(word: lowerAnswer) {
            if isOriginal(word: lowerAnswer) {
                if isReal(word: lowerAnswer) {
                    self.usedWords.insert(answer, at: 0)
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.insertRows(at: [indexPath], with: .automatic)
                    return
                } else {
                    self.showErrorMessage(
                        title: "Word not recognised",
                        message: "You can't just make them up, you know!"
                    )
                }
            }
            else {
                self.showErrorMessage(
                    title: "Word used already",
                    message: "Be more original!"
                )
            }
        } else {
            guard let title = title?.lowercased()
            else {
                return
            }
            self.showErrorMessage(
                title: "Word not possible",
                message: "You can't spell that word from \(title)"
            )
        }
    }
    
    func isOriginal(word: String) -> Bool {
        return !self.usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        guard var tempWord = self.title?.lowercased()
        else {
            return false
        }

        for letter in word {
            if let position = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: position)
            } else {
                return false
            }
        }
        return true
    }

    func isReal(word: String) -> Bool {
        guard word.count > 3 && word.first != self.title?.first
        else {
            return false
        }
        
        let checker = UITextChecker()
        let range = NSRange(
            location: 0,
            length: word.utf16.count
        )
        let misspelledRange = checker.rangeOfMisspelledWord(
            in: word,
            range: range,
            startingAt: 0,
            wrap: false,
            language: "en"
        )
        return misspelledRange.location == NSNotFound
    }
    
    func showErrorMessage(
        title: String,
        message: String
    ) {
        let alertVC = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        alertVC.addAction(
            UIAlertAction(title: "OK", style: .default)
        )
        self.present(alertVC, animated: true)
    }
}

// MARK: - Objc Methods

@objc
private extension WordsTableViewController {
    func startGame() {
        self.title = self.allWords.randomElement()
        self.usedWords.removeAll(keepingCapacity: true)
        self.tableView.reloadData()
    }
    
    func promptForAnswer() {
        let alertVC = UIAlertController(
            title: "Enter answer",
            message: nil,
            preferredStyle: .alert
        )
        alertVC.addTextField()
        
        let submitAction = UIAlertAction(
            title: "Submit",
            style: .default
        ) { [weak self, weak alertVC] action in
            guard let answer = alertVC?.textFields?[0].text
            else {
                return
            }
            self?.submit(answer)
        }
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel
        ) { _ in }
        alertVC.addAction(submitAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true)
    }
}
// MARK: - UITableViewDataSource Conformance

extension WordsTableViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        self.usedWords.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Words",
            for: indexPath
        )
        cell.textLabel?.text = usedWords[indexPath.row]
        return cell
    }
}
